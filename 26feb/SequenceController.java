package com.edatablock.rpa.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;
import com.edatablock.rpa.repository.TransactionRepository;

@RestController
@RequestMapping("/api")
public class SequenceController {
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	@GetMapping("/getProcessIdSequence")
    @Timed
	public ResponseEntity<Integer> getProcessIdSequence() { 
		Integer seq = transactionRepository.findNextSeqProcessId();
		return ResponseEntity.ok().headers(null).body(seq);
	}
}
