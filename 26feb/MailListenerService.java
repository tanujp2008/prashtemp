package com.ebd.mail.component;

import java.io.IOException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.Address;
import javax.mail.FetchProfile;
import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Part;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.ebd.mail.dto.EmailAttachmentDTO;
import com.ebd.mail.dto.EmailMessagesDTO;
import com.ebd.mail.dto.LoginVM;
import com.ebd.mail.dto.TransactionDTO;
import com.ebd.mail.util.Util;

public class MailListenerService {
	
	@Value("${file.download.location}")
	private String fileDownloadLocation;
	
	@Value("${mail.folder.name}")
	private String mailFolderName;
	
	@Value("${ebd.baseurl}")
	private String baseUrl;
	
	@Autowired
	private LoginVM loginVM;
	
	@Autowired
	MailQSender mailQSender;
	
	private EmailMessagesDTO emailMsg;
	
	private List<EmailAttachmentDTO> attachmentList;
	
	boolean isCreateMsg;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	public void handleMail(MimeMessage message) throws Exception {
		Folder folder = message.getFolder();
		folder.open(Folder.READ_WRITE);
		String messageId = message.getMessageID();
		Message[] messages = folder.getMessages();
		FetchProfile contentsProfile = new FetchProfile();
		contentsProfile.add(FetchProfile.Item.ENVELOPE);
		contentsProfile.add(FetchProfile.Item.CONTENT_INFO);
		contentsProfile.add(FetchProfile.Item.FLAGS);
		folder.fetch(messages, contentsProfile);
		TransactionDTO transactionDTO= new TransactionDTO();
		String processId = getProcessSeqId();
		transactionDTO.setProcInstId(processId);
		transactionDTO.setStatus("IN-PROGRESS");
		transactionDTO.setProcessType("EMAIL_RECEIVER");
		transactionDTO.setFileName("");
		transactionDTO.setCreatedBy("SYSTEM");
		for (int i = 0; i < messages.length; i++) {
			if (((MimeMessage) messages[i]).getMessageID().equals(messageId)) {
				MimeMessage mimeMessage = (MimeMessage) messages[i];
				mimeMessage.setFlag(Flags.Flag.SEEN, true);
				logger.info("Message ID:"+mimeMessage.getMessageID());
				logger.info("SUBJECT: " + mimeMessage.getSubject());
				Address senderAddress = mimeMessage.getFrom()[0];
				logger.info("SENDER " + senderAddress.toString());
				String mimeMessageID = mimeMessage.getMessageID();
				InternetAddress senderEmail = (InternetAddress) mimeMessage.getSender();
				String sender = senderEmail.getAddress();
				
				transactionDTO.setClientEmailAddress(sender);
				transactionDTO.setMessageId(mimeMessageID);
				transactionDTO.setDoRun(0);
				//mailQSender.sendTransactionQ(transactionDTO);
				
				if(mimeMessage.getContent() instanceof Multipart) {
					
					attachmentList = new ArrayList<EmailAttachmentDTO>();
					String bodyText = downloadAttachment(mimeMessage, attachmentList);
					
					if(attachmentList.size()>0) {
						StringBuilder fileNames = new StringBuilder();
						attachmentList.forEach(attachment->fileNames.append(attachment.getFileName()).append(","));
						emailMsg = new EmailMessagesDTO();
						emailMsg.setAttachments(fileNames.toString());
						emailMsg.setClientClientEmailAddress(sender);
						emailMsg.setClientEmailAddress(sender);
						//emailMsg.setClientId(1L);
						emailMsg.setEmailBody(bodyText);
						emailMsg.setEmailSubject(mimeMessage.getSubject());
						emailMsg.setMessageId(mimeMessage.getMessageID());
						emailMsg.setReceiveFrom(mimeMessage.getSender().toString());
						emailMsg.setReceivedTime(Instant.now());
						emailMsg.setNumberOfAttachments(attachmentList.size());
						isCreateMsg = true;
						logger.info("No. of Attachments : "+attachmentList.size());
					}
				}
				//break;
			}
		}
		folder.close(false);
		if(isCreateMsg) {
			HashMap<String, Object> emailMsgMap = new HashMap<String, Object>();
			emailMsgMap.put("emailMsg", emailMsg);
			emailMsgMap.put("attachmentList", attachmentList);
			emailMsgMap.put("processId", processId);
			try {
				logger.info("Sending mail to Q.....");
				mailQSender.sendMailToQ(emailMsgMap);
				
				transactionDTO.setStatus("SUCCESS");
				transactionDTO.setDoRun(0);
				mailQSender.sendTransactionQ(transactionDTO);
			} catch (Exception e) {
				logger.error("MailListenerService::handleMail>> ", e);
				transactionDTO.setStatus("FAILURE");
				transactionDTO.setDoRun(1);
				transactionDTO.setRespMsg("FOUND ERROR : "+ (e.getMessage().length()>=200?e.getMessage().substring(0, 200):e.getMessage()));
				mailQSender.sendTransactionQ(transactionDTO);
			}
			emailMsg = null; 
			attachmentList = null;
			isCreateMsg = false;
		} else {
			transactionDTO.setStatus("FAILURE");
			transactionDTO.setRespMsg("No Email Attachment.");
			transactionDTO.setDoRun(0);
			mailQSender.sendTransactionQ(transactionDTO);
			logger.info("Mail does not have attachement......."+transactionDTO.getMessageId());
		}
	}
	
	private String downloadAttachment(MimeMessage mimeMessage, List<EmailAttachmentDTO> attachmentDtoList) throws IOException, MessagingException {

		String bodyText = null;
			Multipart part = (Multipart) mimeMessage.getContent();
			for (int j = 0; j < part.getCount(); j++) {
				MimeBodyPart bodyPart = (MimeBodyPart) part.getBodyPart(j);
				String disposition = bodyPart.getDisposition();
				if(disposition==null) {
					bodyText = getBodyText(bodyPart);
					continue;
				}
				if (disposition != null && Part.ATTACHMENT.equalsIgnoreCase(disposition)) {
					String fileName = bodyPart.getFileName();
					if(fileName.endsWith(".exe")) {
						logger.warn("EXE not allowed......");
						continue;
					}
					bodyPart.saveFile(fileDownloadLocation+fileName);
					EmailAttachmentDTO emailAttachmentDTO = new EmailAttachmentDTO();
					emailAttachmentDTO.setFileName(fileName);
					emailAttachmentDTO.setFileLocation(fileDownloadLocation);
					emailAttachmentDTO.setFileExtension(fileName.substring(fileName.lastIndexOf("."), fileName.length()-1));
					emailAttachmentDTO.setClientEmailAddress(mimeMessage.getSender().toString());
					emailAttachmentDTO.setMessageId(mimeMessage.getMessageID());
					emailAttachmentDTO.setEmailMessagesMessageId(mimeMessage.getMessageID());
					attachmentDtoList.add(emailAttachmentDTO);
					logger.info("File Downloaded : "+fileName);
				}
			}
		return bodyText;
	}
	
	private String getBodyText(Part p) throws MessagingException, IOException {
		if (p.isMimeType("text/*")) {
			String s = (String) p.getContent();
			return s;
		}
		if (p.isMimeType("multipart/*")) {
			Multipart mp = (Multipart) p.getContent();
			for (int i = 0; i < mp.getCount(); i++) {
				String s = getBodyText(mp.getBodyPart(i));
				if (s != null)
					return s;
			}
		}
		return null;
	}
	
	
	public Long getTransactionID(TransactionDTO transactionDTO) {
		RestTemplate rest =new  RestTemplate(); 
		HttpHeaders header = Util.getAuthenticationHeader(rest, baseUrl, loginVM);
		HttpEntity<TransactionDTO> transactionReq = new HttpEntity<>(transactionDTO, header);
		ResponseEntity<TransactionDTO> responseEntity = rest.exchange(baseUrl+"/transactions", HttpMethod.POST, transactionReq, TransactionDTO.class);
		TransactionDTO transactionResponseDTO = responseEntity.getBody();	
		
		return transactionResponseDTO.getId();
	}
	
	public String getProcessSeqId() {
		RestTemplate rest =new  RestTemplate(); 
		HttpHeaders header = Util.getAuthenticationHeader(rest, baseUrl, loginVM);
		HttpEntity<?> transactionReq = new HttpEntity<>(null, header);
		ResponseEntity<Integer> responseEntity = rest.exchange(baseUrl+"/getProcessIdSequence", HttpMethod.GET, transactionReq, Integer.class);
		Integer count = responseEntity.getBody();
		count += 1;
		StringBuilder processId  = new StringBuilder()
				.append("P")
				.append(Util.getCurrentDateStr())
				.append(count);
		return processId.toString();
	}
}
