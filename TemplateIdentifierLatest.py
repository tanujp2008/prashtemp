
import stomp
import time
import sys
import os

import math
from PyPDF2 import PdfFileReader as pfr
from wand.image import Image
from wand.color import Color
import json
import shutil
import datetime
import numpy as np

import cv2
import pytesseract
import pandas as pd
from PIL import Image as Image2
import nltk
from nltk import sent_tokenize

global js, ocr_txt
global clientEmailAddress
global img_folder
global txt
img_folder = '1.Test/Test/'
##img_folder="/home/mprasha/edata_src/edb-ocr-process/Test/"

js = open("C:/Users/ravi.kumar/Desktop/Robotics/Others/Listener/fwdocrdatajsonmodel/OCRProcessedDataJSON - Copy.txt").read()
##js = open("/home/mprasha/edata_src/edb-ocr-process/OCRProcess.txt").read()

ocr_txt = json.loads(js)
global inputMsg
inputMsg = {}
text = None

user = os.getenv("ACTIVEMQ_USER") or "Admin"
password = os.getenv("ACTIVEMQ_PASSWORD") or "Admin"
host = os.getenv("ACTIVEMQ_HOST") or "localhost"
port = os.getenv("ACTIVEMQ_Port") or 61616
destination = sys.argv[1:2] or ["DLQ"] ##["/queue/EDB.OUTBOUNDOCRPROCESS"]
destination = destination[0]

D_time = str(datetime.datetime.now())
ErrorMsg = [{"Errorcode":"100",
             "Description":"",
             "process_type":"OCR_PROCESS",
             "createdate":"",
             "Exception":"",
             "ProcessBy":"",
             "processId":"",
             "uniqueidmessage":""}]



##*********************** Listener *****************************

class SampleListener(object):
    def on_message(self, headers, msg):
        global text
        text = msg
user = os.getenv("ACTIVEMQ_USER") or "Admin"
password = os.getenv("ACTIVEMQ_PASSWORD") or "Admin"
host = os.getenv("ACTIVEMQ_HOST") or "localhost" ##"104.198.143.255"
port = int(os.getenv("ACTIVEMQ_PORT") or 61616)
destination = sys.argv[1:2] or ["DLQ"] ##["/queue/EDB.INBOUNDOCRPROCESS"]
destination = destination[0]
conn = stomp.Connection(host_and_ports = [(host,port)])
conn.set_listener('SampleListener', SampleListener())
conn.start()
conn.connect(login=user, passcode = password)
conn.subscribe(destination=destination, id=1, ack='auto')
time.sleep(5)
conn.disconnect()

##**************************** Listener END ****************************


##**************************** Sender START ****************************


def Sender_to_Queue(inputMsg):
    import time
    import sys
    import os
    import stomp

    messages = 1
    conn = stomp.Connection(host_and_ports = [(host,port)])
    conn.start()
    conn.connect(login=user, passcode = password)
    print("*************Sending OutputFinal ***********************")

    print(json.dumps(inputMsg))

    for i in range(0,messages):
        conn.send(body = json.dumps(inputMsg),destination = destination, persistent = 'false')
    conn.disconnect()


##******************************* Sender End *****************************************

##**************************** Error Queue START ****************************

##Added on 12/02/2018

def Error_Queue(ErrorMsg):
    import time
    import sys
    import os
    import stomp

    user = os.getenv("ACTIVEMQ_USER") or "Admin"
    password = os.getenv("ACTIVEMQ_PASSWORD") or "Admin"
    host = os.getenv("ACTIVEMQ_HOST") or "localhost" ##"104.198.143.255"
    port = os.getenv("ACTIVEMQ_Port") or 61616  ##61613
    destination = sys.argv[1:2] or ["DLQ"]     ##["/queue/event"]
    destination = destination[0]
    messages = 1
    conn = stomp.Connection(host_and_ports = [(host,port)])
    conn.start()
    conn.connect(login=user, passcode = password)
    print("*************Sending Error Output ***********************")
    final_data = ErrorMsg
    print(final_data)
        
    for i in range(0,messages):
        conn.send(body = json.dumps(final_data),destination = destination, persistent = 'false')
    ##    conn.send("SHUTDOWN",destination = destination, persistent = 'false')
    conn.disconnect()


##******************************* Error Queue End *****************************************


def move_files(path,moveto):
    files = os.listdir(path)
    files.sort()
    for f in files:
        src = path + f
        dst = moveto + f
        shutil.move(src,dst)



##******************************* PDF to IMAGE *****************************************

def create_img(ipfile):

##    ipfile = fileLocation
    print("Execution Started")
    print("*****************************************************************")
    print("pdf to image conversion in progress...")
    print(" ")    
#Read file and count pages
    try:
        global reader
        reader = pfr(open(ipfile,'rb')) 
    except FileNotFoundError:
        ErrorMsg = [{"Errorcode":"100",
             "Description":"Please check might be file or folder location is incorrect",
             "process_type":"OCR_PROCESS",
             "createdate":D_time,
             "Exception":"",
             "ProcessBy":"",
             "processId":"",
             "uniqueidmessage":"",
             "inputmsg":txt}]
        Error_Queue(ErrorMsg)
        sys.exit()

    page_count = reader.getNumPages()

    images = []

    with Image(filename= ipfile, resolution=300) as img:
            img.compression_quality = 100
            img.background_color = Color('white')
            img.alpha_channel='remove'
##            img.save(filename=img_folder+'img.jpg')
            img.save(filename=img_folder+fileName+'.jpg')
        
    print("PDF converted to image:")
    print(" ")
    print("*****************************************************************")
    print("Please wait while converting selection to text:")
    print(" ")
    print("*****************************************************************")
    pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
##Selection from image with given coordinates
    images = []
    for img_file in os.listdir(img_folder):
            img_file = os.path.join(img_folder,img_file)
            images.append(img_file)

##***************************************************************************

##************** Extract Values from Listener *****************************

def doOCR(text):
        
    coordinates = ["fieldZoneMinX","fieldZoneMinY","fieldZoneMaxX","fieldZoneMaxY","width","height"]

    global txt
    try:
        txt = json.loads(text)
    except TypeError:
        D_time = str(datetime.datetime.now())
        ErrorMsg = [{"Errorcode":"101",
                 "Description":"Found TypeError: Please check might be Queue is empty!",
                 "process_type":"OCR_PROCESS",
                 "createdate":D_time,
                 "Exception":"",
                 "ProcessBy":"",
                 "processId":"",
                 "uniqueidmessage":"",
                 "inputmsg":"Input message is not available"}]
        Error_Queue(ErrorMsg)
        sys.exit()

    ##********** FileLocation and if template is matching *******************

    check = txt["emailAttachmentDataList"]

    j = 0
    for i in check:
        j += 1
        c = check[j-1]
        attachmentData = c["attachmentData"]
        varCheck = c["isTemplateMatch"]

        try:
            global TemplateID
            TemplateID = c["templateId"]
        except KeyError:
            ErrorMsg = [{"Errorcode":"106",
                 "Description":"Please check might be templateId is not available",
                 "process_type":"OCR_PROCESS",
                 "createdate":D_time,
                 "Exception":"",
                 "ProcessBy":"",
                 "processId":"",
                 "uniqueidmessage":"",
                 "inputmsg":txt}]
            Error_Queue(ErrorMsg)
            sys.exit()
        
        
        if varCheck != True and (TemplateID == None or TemplateID != None):

            for data in attachmentData:
                global fileName,clientEmailAddress,messageId,fileLocation,fileExtension
                if data == "fileName":
                    fileName = attachmentData["fileName"]
                elif data == "fileLocation":
                    fileLocation = attachmentData["fileLocation"]
                elif data == "fileExtension":
                    fileExtension = attachmentData["fileExtension"]
                elif data == "id":
                    allTemplateFieldIndicator = attachmentData["id"]
                elif data == "messageId":
                    messageId = attachmentData["messageId"]
                elif data == "clientEmailAddress":
                    clientEmailAddress = attachmentData["clientEmailAddress"]

            if fileName[-4:].lower() == '.pdf':
                fileLocation = attachmentData["fileLocation"] + attachmentData["fileName"]
            else:
                ErrorMsg = [{"Errorcode":"102",
                         "Description":"PDFError: Might be received file is other than pdf file",
                         "process_type":"OCR_PROCESS",
                         "createdate":D_time,
                         "Exception":"",
                         "ProcessBy":"",
                         "processId":"",
                         "uniqueidmessage":"",
                         "inputmsg":txt}]
                Error_Queue(ErrorMsg)
                sys.exit()
                
            create_img(fileLocation)

            img_location_with_page = img_folder+fileName+'.jpg'
            image = cv2.imread(img_location_with_page)
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            kernal = np.ones((1,1),np.uint8)
            gray_image = cv2.dilate(gray_image, kernal, iterations = 1)
            gray_image = cv2.erode(gray_image,kernal, iterations = 1)
    #Roman
            os.environ['PATH'] = os.environ['PATH'].encode('utf-8')
            Temp_text = pytesseract.image_to_string(gray_image)
            Temp_text = text.replace('\n',' ')
            sentance = nltk.sent_tokenize(Temp_text)

            allTemplate = txt["allTemplate"]
            for T_id in allTemplate:
                for tidentifier in allTemplate[T_id]:
                    if tidentifier == "templateIdentifier":
                        templateIdentifier = allTemplate[T_id]["templateIdentifier"]
                    elif tidentifier == "templateType":
                        templateType = allTemplate[T_id]["templateType"]
                    elif tidentifier == "templateName":
                        templateName = allTemplate[T_id]["templateName"]
                    elif tidentifier == "templateDescription":
                        templateDescription = allTemplate[T_id]["templateDescription"]

                if templateIdentifier != "":
                    
                    for line in sentance:
                        for word in nltk.word_tokenize(line):
                            if word == templateIdentifier:
                                txt2 = txt["allTemplateFields"]
                                for s in txt2:
                                    if int(s) == int(T_id):
                                        templateId = int(T_id)
                                        txt3 = txt2[s]
        ##                                global final, page_no
                                        final = {}
                                        page_no = {}
                                        for n in txt3:
                                            final[n["fieldName"]] = [n[q] for q in coordinates]
                                            page_no[n["fieldName"]] = n["pageNumebr"]

                                        create_img(fileLocation)

                                        attachmentData["templateType"] = templateType

                                        c["isTemplateMatch"] = "true"
                                        c["templateId"] = int(T_id)
                                        
                                        path = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Test/"
                                        moveto = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Archieve/"
                                        move_files(path,moveto)


    ##                            Sender_to_Queue(text)

                else:
                    for keys in txt["allTemplateFields"]:
                        for values in txt["allTemplateFields"][keys]:
                            if values["isTemplateIdentifier"] == "y":
                                
                                fieldname = values["fieldName"]
                                pn = values["pageNumebr"]
                                x1 = int(values["fieldZoneMinX"])
                                y1 = int(values["fieldZoneMinY"])
                                x2 = int(values["fieldZoneMaxX"])
                                y2 = int(values["fieldZoneMaxY"])
                                w = int(values["width"])
                                h = int(values["height"])

                                if pn <2:
                                    gray_image = gray_image
                                else:
                                    img_location_with_page = img_folder+fileName+'-'+str(pn-1)+'.jpg'
                                    image = cv2.imread(img_location_with_page)
                                    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

                                crop_img = gray_image[y1:y1+h,x1:x1+w]
                                kernal = np.ones((1,1),np.uint8)
                                crop_img = cv2.dilate(crop_img, kernal, iterations = 1)
                                crop_img = cv2.erode(crop_img,kernal, iterations = 1)
    #Roman
                                os.environ['PATH'] = os.environ['PATH'].encode('utf-8')
                                text = pytesseract.image_to_string(crop_img)
                                text = text.replace('\n',' ')
                                print(keys)
                                if text.find(fieldname) >= 0:

                                    attachmentData["templateType"] = templateType
                                    c["isTemplateMatch"] = "true"
                                    c["templateId"] = int(T_id)

                                else:
                                    

                                    ErrorMsg = [{"Errorcode":"104",
                                                "Description":"Template not found",
                                                "process_type":"OCR_PROCESS",
                                                "createdate":D_time,
                                                "Exception":"",
                                                "ProcessBy":"",
                                                "processId":"",
                                                "uniqueidmessage":"",
                                                "inputmsg":txt}]
                                    Error_Queue(ErrorMsg)
                                    sys.exit()                                

                                path = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Test/"
                                moveto = "C:/Users/ravi.kumar/Desktop/Robotics/Others/1.Test/Archieve/"
                                move_files(path,moveto)

        elif varCheck == True and TemplateID == None:

            ErrorMsg = [{"Errorcode":"105",
                        "Description":"TemplateId is null for isTemplateMatch true",
                        "process_type":"OCR_PROCESS",
                        "createdate":D_time,
                        "Exception":"",
                        "ProcessBy":"",
                        "processId":"",
                        "uniqueidmessage":"",
                        "inputmsg":txt}]
            Error_Queue(ErrorMsg)
            sys.exit() 
                    
    Sender_to_Queue(txt)
