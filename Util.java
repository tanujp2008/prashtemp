/**
 * 
 */
package com.ebd.mail.util;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.ebd.mail.dto.LoginVM;

/**
 * @author dell
 *
 */
public final class Util {
	
	private Util() {}
	
	public static HttpHeaders getAuthenticationHeader(RestTemplate rest, String baseUrl, LoginVM loginVM){
		HttpHeaders headers = null;
		try {
			HttpEntity<LoginVM> httpRequest = new HttpEntity<>(loginVM);
			ResponseEntity<?> response = rest.exchange(baseUrl+"/authenticate", HttpMethod.POST, httpRequest, Object.class);
			headers = new HttpHeaders();
			headers.add("Authorization", response.getHeaders().get("Authorization").get(0));
			headers.add("Content-Type", "application/json");
		} catch (RestClientException e) {
			throw e;
		}
		return headers;
	}
	
	public static String getCurrentDateStr() {
		SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
		String currentDate = format.format(new Date());
		return currentDate;
	}

}
