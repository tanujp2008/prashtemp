import os
import sys
import time
import json
import mysql.connector
import datetime

import time


from stompest.async import Stomp
from stompest.async.listener import SubscriptionListener
from stompest.config import StompConfig
from twisted.internet import defer, reactor
import logging
import logging.config

user = os.getenv('ACTIVEMQ_USER') or 'admin'
password = os.getenv('ACTIVEMQ_PASSWORD') or 'admin'
host = os.getenv('ACTIVEMQ_HOST') or '104.198.143.255'
port = int(os.getenv('ACTIVEMQ_PORT') or 61613)
destination = sys.argv[1:2] or ['/queue/EDB.TRANSACTIONS']
destination = destination[0]


logging.basicConfig(level=logging.INFO)
log = logging.getLogger(__name__)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh = logging.FileHandler(filename='ocrlog.log')
fh.__class__("logging.handlers.RotatingFileHandler")
fh.setLevel(logging.DEBUG)
fh.setFormatter(formatter)

log.addHandler(fh)

cnx = mysql.connector.connect(user="root",host="localhost",passwd="root",
                              database="edatablockrpamain")

class Listener(object):
    @defer.inlineCallbacks
    def run(self):
        config = StompConfig('tcp://%s:%d' % (host, port), login=user, passcode=password, version='1.1')
        client = Stomp(config)
        yield client.connect(host='mybroker')

        self.count = 0
        self.start = time.time()

        client.subscribe(destination, listener=SubscriptionListener(self.handleFrame),
                         headers={'ack': 'auto', 'id': 'required-for-STOMP-1.1'})

    @defer.inlineCallbacks
    def handleFrame(self, client, frame):
        self.count += 1
        log.info("Message Recieved..")
        main(frame.body)

    @defer.inlineCallbacks
    def stop(self, client):
        log.info('Disconnecting. Waiting for RECEIPT frame ...'),
        yield client.disconnect(receipt='bye')
        log.info('Queue connection stopped - ok')

        diff = time.time() - self.start
        # print 'Received %s frames in %f seconds' % (self.count, diff)
        reactor.stop()
        sys.exit()


def insertTrandata(message):

    try:
        if cnx.is_connected() :
            cur = cnx.cursor()
            log.info('inside db')

            sql = "insert into transactions(proc_inst_id,status," \
                  "client_email_address,messageid,processtype,dorun" \
                  ",client_id,remarks,respmsg) values(%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            val = (message['processInstID'],message['status'],message['clientEmailAddress'],message['messageId']
                   ,message['process_type'],message['dorun'],message['client'],message['respmsg'],message['inputmsg'])
            #cur.execute(sql, (json.dumps(message),))
            cur.execute(sql,val)
            #cnx.commit()
            cur.close()
        else :
            log.error("DB Connection is not connected!!")

        log.info("insertTrandata:Data inserted into Transaction table!!")
    except:
        log.error("Error:insertTrandata:Unexpected error:", sys.exc_info()[0])
    finally:
        log.info("insertTrandata:finally Done")


def  main(message):
    try:
        log.info("start:main - started")
        log.info("message %s", message)

        Listener().run()
        reactor.run()

        log.info("start:main - completed ")
    except:
        log.error("Unexpected error:", sys.exc_info()[0])
    finally:
        log.info("Finally - Main Operation completed ")
