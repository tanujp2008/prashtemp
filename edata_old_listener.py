import os
import sys
import time
import json
from PyPDF2 import PdfFileReader as pfr
from pandas.compat import FileNotFoundError
from wand.image import Image
from wand.color import Color
import json
import shutil
import datetime
import numpy as np

import cv2
import pytesseract
import pandas as pd
from PIL import Image as Image2
import nltk
from nltk import sent_tokenize
import json
import time

from stompest.async import Stomp
from stompest.async.listener import SubscriptionListener
from stompest.config import StompConfig
from twisted.internet import defer, reactor

user = os.getenv('ACTIVEMQ_USER') or 'admin'
password = os.getenv('ACTIVEMQ_PASSWORD') or 'admin'
host = os.getenv('ACTIVEMQ_HOST') or '104.198.143.255'
port = int(os.getenv('ACTIVEMQ_PORT') or 61613)
destination = sys.argv[1:2] or ['/queue/EDB.INBOUNDDOCPROCESS']
destination = destination[0]

messages = 10000
coordinates = ["fieldZoneMinX","fieldZoneMinY","fieldZoneMaxX","fieldZoneMaxY","width","height"]

file_list = []

class Listener(object):
    @defer.inlineCallbacks
    def run(self):
        config = StompConfig('tcp://%s:%d' % (host, port), login=user, passcode=password, version='1.1')
        client = Stomp(config)
        yield client.connect(host='mybroker')

        self.count = 0
        self.start = time.time()

        client.subscribe(destination, listener=SubscriptionListener(self.handleFrame),
                         headers={'ack': 'auto', 'id': 'required-for-STOMP-1.1'})

    @defer.inlineCallbacks
    def handleFrame(self, client, frame):
        self.count += 1
        print("Message Recieved..")
        main(frame.body)
        if self.count == messages:
            self.stop(client)

    @defer.inlineCallbacks
    def stop(self, client):
        print('Disconnecting. Waiting for RECEIPT frame ...'),
        yield client.disconnect(receipt='bye')
        print('ok')

        diff = time.time() - self.start
        # print 'Received %s frames in %f seconds' % (self.count, diff)
        reactor.stop()
        sys.exit()

def sendmessage(inputMsg):
    import stomp

    print("Sender_to_Queue:Recieved message")
    try:
        conn = stomp.Connection(host_and_ports=[(host, port)])
        conn.start()
        conn.connect(login=user, passcode=password)
        print("*************Sending OutputFinal ***********************")

        print(json.dumps(inputMsg))
        dest = '/queue/OCRProcessInboundPDF'
        conn.send(body=json.dumps(inputMsg), destination=dest, persistent='false')
        conn.disconnect()
        print("Message Sent")

    except:
        print("Error:Sender_to_Queue")

def create_img(fileLocation, fileName, fileIdentifier):

    ipfile = fileLocation
    print("Execution Started")
    print("***************************************************************** b")
    print("pdf to image conversion in progress...")
    print(" ")
    # Read file and count pages
    try:

        reader = pfr(open(ipfile, 'rb'))
    except FileNotFoundError:
        D_time = str(datetime.datetime.now())
        print("Error:create_img:Template file Not found")
        #Error_Queue(ErrorMsg)
        print("Msg Sent to Queue")
        #sys.exit()

    page_count = reader.getNumPages()
    print(page_count)

    img_folder = '/Users/apple/Downloads/temp_image/'

    images = []
    print(ipfile,fileName[:-4])


    with Image(filename=ipfile, resolution=300) as img:
        img.compression_quality = 100
        img.background_color = Color('white')
        img.alpha_channel = 'remove'

        img.save(filename=img_folder + fileName[:-4].replace(" ","") + '.jpg')
        strtemp = {'inputTemplateId':  fileIdentifier ,
                 'filename':  img_folder +fileName[:-4].replace(" ","") + ".jpg"}

        file_list.append(strtemp)

    print("*****************************************************************")
    print("PDF converted to image:")
    print(file_list)
    print("*****************************************************************")


def doextract_text(txt) :
    print("doextract:starts..")
    strJson = {}
    try:
        for keys in txt["allTemplateFields"]:
            for values in txt["allTemplateFields"][keys]:

                pn = values["pageNumebr"]
                result = list(
                    filter(lambda x: (int(x['inputTemplateId']) == int(values["inputTemplateId"])), file_list))

                strfile = str(result[0]['filename'])
                strfile = strfile[:-4] + '-' + str(pn - 1) + strfile[-4:]
                fieldname = str(values["fieldName"])

                if int(values["isTemplateIdentifier"]) == 0:
                 #

                    x1 = int(values["fieldZoneMinX"])
                    y1 = int(values["fieldZoneMinY"])
                    x2 = int(values["fieldZoneMaxX"])
                    y2 = int(values["fieldZoneMaxY"])
                    w = int(values["width"])
                    h = int(values["height"])
                    #print(fieldname,str(values["inputTemplateId"]),file_list)

                    # This needs to be changed to use single image and crop multiple fields
                    image = cv2.imread(strfile)
                    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    crop_img = gray_image[y1:y1 + h, x1:x1 + w]
                    kernal = np.ones((1, 1), np.uint8)
                    crop_img = cv2.dilate(crop_img, kernal, iterations=1)
                    crop_img = cv2.erode(crop_img, kernal, iterations=1)

                    os.environ['PATH'] = os.environ['PATH'].encode('utf-8')
                    text = pytesseract.image_to_string(crop_img)
                    text = text.replace('\n', ' ')

                    strJson[values['fieldName']] = text
                    print(values['fieldName'],text)
                else:

                    image = cv2.imread(strfile)
                    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
                    kernal = np.ones((1, 1), np.uint8)
                    gray_image = cv2.dilate(gray_image, kernal, iterations=1)
                    gray_image = cv2.erode(gray_image, kernal, iterations=1)
                    # Roman
                    os.environ['PATH'] = os.environ['PATH'].encode('utf-8')
                    Temp_text = pytesseract.image_to_string(gray_image)
                    Temp_text = Temp_text.replace('\n', ' ')
                    sentance = nltk.sent_tokenize(Temp_text)

                    c_fields_token = nltk.word_tokenize(fieldname)
                    intwords = int(values['fieldValidationRule']);

                    for line in sentance:
                        if (unicode(line).__contains__(unicode(fieldname))):
                            c_sentance = nltk.word_tokenize(line)

                            print(c_sentance)
                            position = 0
                            indexposition = 0
                            print(line)

                            for word in nltk.word_tokenize(line):
                                position += 1
                                # print(line)
                                if (len(word) > 1 and c_fields_token.__contains__(word)):
                                    indexposition = position
                                    endpos = (position + len(c_fields_token));

                                    if (endpos <= len(c_sentance)):
                                        p_list = c_sentance[position - 1:endpos - 1]
                                        c_list = c_fields_token[:]

                                        if (c_list == p_list):
                                            strtext= c_sentance[endpos - 1:endpos - 1 + intwords]
                                            s=" "
                                            strJson[values['fieldName']] = s.join(strtext)
                                            print("Field Identifier ",s.join(strtext))

                    print("Error:No template identifier", values)

        sendmessage(strJson)
        print("Process Completed")

    except TypeError:
        print("Error:doextract")


def doPrepare(txt) :
    try:
        check = txt["emailAttachmentDataList"]


        j = 0
        for i in check:
            j += 1
            c = check[j - 1]
            attachmentData = c["attachmentData"]
            varCheck = c["isTemplateMatch"]

            if varCheck == True:
                print("It is true part")
                templateId = c["templateId"]
                for data in attachmentData:
                    if data == "fileName":
                        fileName = attachmentData["fileName"]
                        print(fileName)
                    elif data == "fileLocation":
                        fileLocation = attachmentData["fileLocation"]
                    elif data == "fileExtension":
                        fileExtension = attachmentData["fileExtension"]
                    elif data == "id":
                        allTemplateFieldIndicator = attachmentData["id"]
                    elif data == "messageId":
                        messageId = attachmentData["messageId"]
                    elif data == "clientEmailAddress":
                        clientEmailAddress = attachmentData["clientEmailAddress"]

                if fileName[-4:].lower() == '.pdf':
                    fileLocation = attachmentData["fileLocation"] + attachmentData["fileName"]
                    #Hard coded for testing
                    fileLocation="/Users/apple/Downloads/" + attachmentData["fileName"]
                else:
                    D_time = str(datetime.datetime.now())
                    print("Error: while getting File Location!!")

                #print(fileName, fileLocation, fileExtension, allTemplateFieldIndicator, messageId, clientEmailAddress)
                txt2 = txt["allTemplateFields"]

                for k in txt2:
                    if int(k) == templateId:
                        txt3 = txt2[k]
                        final = {}
                        page_no = {}
                        if varCheck == True:
                            for n in txt3:
                                final[n["fieldName"]] = [n[c] for c in coordinates]
                                page_no[n["fieldName"]] = n["pageNumebr"]

                        create_img(fileLocation,fileName,templateId)
            else:
                print("Else No template found!!")
    except TypeError:
        D_time = str(datetime.datetime.now())
        ErrorMsg = [{"Errorcode": "101",
                     "Description": "Found TypeError: Please check might be Queue is empty!",
                     "process_type": "OCR_PROCESS_ERROR",
                     "createdate": D_time,
                     "Exception": "",
                     "ProcessBy": "",
                     "processId": "",
                     "uniqueidmessage": "",
                     "inputmsg": txt}]
        # Send to Error Queue
        print("Error:doPrepare:exception while parsing/processing message")

def main(text):
    print("main:process starts..")
    try:
        txt = json.loads(text)
        print("doocr:starts..")
        doPrepare(txt)
        doextract_text(txt)
        print("doocr:Process completed..")
    except TypeError:
        D_time = str(datetime.datetime.now())
        ErrorMsg = [{"Errorcode": "101",
                     "Description": "Found TypeError: Please check might be Queue is empty!",
                     "process_type": "OCR_PROCESS_ERROR",
                     "createdate": D_time,
                     "Exception": "",
                     "ProcessBy": "",
                     "processId": "",
                     "uniqueidmessage": "",
                     "inputmsg": text}]
        #Send to Error Queue
        print("Error:main:exception while parsing/processing message")
    print("main:process ends..")

if __name__ == '__main__':
    print("Process started")
    Listener().run()
    reactor.run()



    #doPrepare("Test") #Actual message
    #doextract_text(txt)
    #Listener().run()
    #reactor.run()